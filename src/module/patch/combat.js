import { MusicController } from "../music-controller.js";

export const patchCombat = function () {
  const clsPath = "CONFIG.Combat.documentClass";

  /* -------------------------------------- */
  /* setupTurns
  /* -------------------------------------- */
  libWrapper.register("vgmusic", `${clsPath}.prototype.setupTurns`, function (wrapped, ...args) {
    const result = wrapped(...args);

    // Refresh combat music
    MusicController.playCurrentTrack();

    return result;
  });
};
