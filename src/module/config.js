export const C = {
  // Scene playlist sections
  scenePlaylistSections: {
    area: "VGMusic.Scene.PlaylistSection.Area",
    combat: "VGMusic.Scene.PlaylistSection.Combat",
  },

  // Actor playlist sections
  actorPlaylistSections: {
    combat: "VGMusic.Scene.PlaylistSection.Combat",
  },
};
