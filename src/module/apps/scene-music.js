import { MusicController } from "../music-controller.js";

export class SceneMusicConfig extends FormApplication {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      title: game.i18n.localize("VGMusic.SceneMusic"),
      classes: ["scene-music"],
      width: 480,
      height: 360,
      template: "modules/vgmusic/templates/apps/scene-music-config.hbs",
      closeOnSubmit: false,
      submitOnClose: false,
      submitOnChange: false,
      resizable: true,
      dragDrop: [{ dropSelector: ".playlist" }],
    });
  }

  async _onDrop(event) {
    event.preventDefault();
    const section = event.currentTarget.dataset.section;

    const data = JSON.parse(event.dataTransfer.getData("text/plain"));
    if (data.type !== "Playlist") return;

    const prio = section === "combat" ? -10 : -20;

    await this.object.unsetFlag("vgmusic", `music.${section}`);
    await this.object.setFlag("vgmusic", `music.${section}`, {
      playlist: data.id,
      priority: prio,
      initialTrack: "",
    });
    this.render();
  }

  async getData() {
    const data = await super.getData();

    // Add playlist sections
    const sections = CONFIG.VGMusic.scenePlaylistSections;
    data.playlists = Object.entries(sections).map((o) => {
      const [k, v] = o;
      const playlist = game.playlists.get(this.object.getFlag("vgmusic", `music.${k}.playlist`));
      const tracks = (playlist?.playbackOrder ?? []).map((id) => {
        const track = playlist.sounds.get(id);
        return {
          id,
          name: track.name,
        };
      });

      return {
        key: k,
        label: v,
        playlist,
        tracks,
        data: this.object.getFlag("vgmusic", `music.${k}`),
        allowPriority: true,
      };
    });

    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find(".playlist .control .delete").on("click", this._onDeletePlaylist.bind(this));
    html.find(`*[data-action="open-playlist"]`).on("click", this._onOpenPlaylist.bind(this));
    html.find(`*[name="initialTrack"]`).on("change", this._onChangeInitialTrack.bind(this));
    html.find(`*[name="priority"]`).on("change", this._onChangePriority.bind(this));
  }

  async _onDeletePlaylist(event) {
    event.preventDefault();
    const section = event.currentTarget.closest(".playlist").dataset.section;
    await this.object.unsetFlag("vgmusic", `music.${section}`);
    this.render();
  }

  _onOpenPlaylist(event) {
    event.preventDefault();

    const playlistId = event.currentTarget.closest(".playlist").dataset.itemId;
    const playlist = game.playlists.get(playlistId);
    if (playlist) playlist.sheet.render(true);
  }

  async _onChangeInitialTrack(event) {
    event.preventDefault();

    const playlistId = event.currentTarget.closest(".playlist").dataset.itemId;
    await this.object.unsetFlag("vgmusic", `playlist.${playlistId}`);

    const section = event.currentTarget.closest(".playlist").dataset.section;
    const trackId = event.currentTarget.value;
    await this.object.setFlag("vgmusic", `music.${section}.initialTrack`, trackId);

    // Change music if this scene is active
    if (this.object.active && MusicController.scope === "scene") {
      await MusicController.music?.stop();
      MusicController.playCurrentTrack();
    }

    this.render();
  }

  async _onChangePriority(event) {
    event.preventDefault();
    const value = parseInt(event.currentTarget.value);
    if (!Number.isNumeric(value)) return;
    const section = event.currentTarget.closest(".playlist").dataset.section;
    await this.object.setFlag("vgmusic", `music.${section}.priority`, value);
    this.render();
  }

  close(...args) {
    MusicController.playCurrentTrack();
    return super.close(...args);
  }
}
