import { SyncStorage } from "./sync-storage.js";
import { isHeadGM } from "./lib.js";
import { MusicController } from "./music-controller.js";

export const registerSockets = function () {
  game.socket.on("module.vgmusic", (msg, userId) => {
    socketDictionary[msg.type](...(msg.args ?? []), userId);
  });
};

const socketDictionary = {
  sync: function (playlistId, trackId, playbackData, timeout = null) {
    SyncStorage.add(playlistId, trackId, playbackData, timeout);
  },

  connect: function (userId) {
    // Sync audio when a user connects
    if (userId !== game.user.id && isHeadGM()) MusicController.sync({ timeout: 15000 });
  },
};
