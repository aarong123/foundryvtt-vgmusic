export const SyncStorage = {
  _db: [],

  add(playlistId, trackId, playbackData, timeout = 2500) {
    const entry = new SyncStorageEntry(playlistId, trackId, playbackData);
    this._db.push(entry);
    entry.activateListeners();

    if (timeout > 0 && !entry._destroyed) {
      window.setTimeout(() => {
        entry.delete();
      }, timeout);
    }
  },
};

export class SyncStorageEntry {
  constructor(playlistId, trackId, playbackData) {
    this.playlistId = playlistId;
    this.trackId = trackId;
    this.playbackData = playbackData;
    this.handler = null;

    this._destroyed = false;
  }

  activateListeners() {
    // Fire immediately if already loaded
    if (this.sound?.loaded && this.sound?.playing) {
      this.syncSound();
      return;
    }

    // Add listener for when sound starts playing
    this.handler = function () {
      if (!this.sound) return;
      if (this._destroyed) return;

      this.syncSound();
    };
    this.sound?.on("start", this.handler.bind(this), { once: true });
  }

  syncSound() {
    // Delete handler
    this.delete();

    // Set playback data
    this.sound.play(this.playbackData);
  }

  get playlist() {
    return game.playlists.get(this.playlistId);
  }

  get track() {
    return this.playlist?.sounds.get(this.trackId);
  }

  get sound() {
    return this.track?.sound;
  }

  delete() {
    if (this._destroyed) return;

    this._destroyed = true;
    if (this.handler != null) {
      this.sound?.off("start", this.handler);
    }
    SyncStorage._db = SyncStorage._db.filter((o) => o !== this);
  }
}
