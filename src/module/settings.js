export function registerSettings() {
  const modName = "vgmusic";

  /**
   * Fade Duration
   */
  game.settings.register(modName, "fadeDuration", {
    name: "VGMusic.SETTINGS.FadeDuration.Name",
    hint: "VGMusic.SETTINGS.FadeDuration.Hint",
    scope: "world",
    config: true,
    type: Number,
    range: {
      min: 500,
      max: 5000,
      step: 100,
    },
    default: 1000,
  });

  /**
   * Silent Combat Music
   */
  game.settings.register(modName, "silentCombatMusicMode", {
    name: "VGMusic.SETTINGS.SilentCombatMusicMode.Name",
    hint: "VGMusic.SETTINGS.SilentCombatMusicMode.Hint",
    scope: "world",
    config: true,
    type: String,
    choices: {
      highestPriority: "Play music of the actor with highest priority",
      lastActor: "Play combat music of the last actor with one",
      area: "Play the area music",
    },
    default: "highestPriority",
    onChange: () => {
      game.VGMusic.MusicController.playCurrentTrack();
    },
  });

  game.settings.register(modName, "supress.area", {
    name: "supress.area",
    scope: "world",
    config: false,
    type: Boolean,
    default: false,
    onChange: () => {
      game.VGMusic.MusicController.playCurrentTrack();
    },
  });
  game.settings.register(modName, "supress.combat", {
    name: "supress.combat",
    scope: "world",
    config: false,
    type: Boolean,
    default: false,
    onChange: () => {
      game.VGMusic.MusicController.playCurrentTrack();
    },
  });
}
