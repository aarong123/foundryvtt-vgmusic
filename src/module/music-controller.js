import { isHeadGM } from "./lib.js";
import { SyncStorage } from "./sync-storage.js";
import { PlaylistContext } from "./playlists.js";

export const MusicController = {
  music: null,
  context: null,

  get currentCombat() {
    return game.combats.find((o) => o.scene === this.currentScene);
  },
  get currentScene() {
    return game.scenes.find((o) => o.active);
  },
  get fadeDuration() {
    return game.settings.get("vgmusic", "fadeDuration");
  },

  getAllCurrentPlaylists() {
    let result = [];

    // Get scene area playlist
    const scene = this.currentScene;
    if (scene != null) {
      const ctx = PlaylistContext.fromDocument(scene, "area", scene);
      if (ctx) result.push(ctx);
    }

    // Get combat playlists
    const combat = this.currentCombat;
    if (scene != null) {
      const ctx = PlaylistContext.fromDocument(scene, "combat", combat);
      if (ctx) result.push(ctx);
    }

    // Get actor combat playlists
    if (combat != null) {
      for (const combatant of combat.combatants) {
        const ctx = PlaylistContext.fromDocument(combatant.actor, "combat", combat);
        if (ctx) result.push(ctx);
      }
    }

    return result;
  },

  filterPlaylists(ctx) {
    const combat = this.currentCombat;
    // Remove combat track without combat
    if (ctx.context === "combat" && !combat?.started) return false;
    // Remove combat track with combat music supressed
    if (ctx.context === "combat" && game.settings.get("vgmusic", "supress.combat") === true) return false;

    // Remove area track with area music supressed
    if (ctx.context === "area" && game.settings.get("vgmusic", "supress.area") === true) return false;

    return true;
  },

  sortPlaylists(a, b) {
    // Sort by current combatant
    const combat = this.currentCombat;
    const currentActor = combat?.combatant.actor;
    if (a.contextEntity === currentActor) return -1;
    if (b.contextEntity === currentActor) return 1;

    // Sort to make sure last actor's combat music will be playing
    if (combat?.started) {
      const hasSceneCombatMusic = PlaylistContext.fromDocument(this.currentScene)?.playlist != null;
      const silentMode = game.settings.get("vgmusic", "silentCombatMusicMode");
      if (silentMode === "lastActor" && !hasSceneCombatMusic) {
        const combatants = combat.turns;
        const startIdx = combat.current.turn;
        if (startIdx >= 0 && combatants.length > 0) {
          let i = startIdx;
          while (i !== (startIdx + 1) % combatants.length) {
            i--;
            if (i < 0) i += combatants.length;

            const actor = combatants[i].actor;
            if (a.contextEntity === actor) return -1;
            if (b.contextEntity === actor) return 1;
          }
        }
      } else if (silentMode === "area" && !hasSceneCombatMusic) {
        if (a.contextEntity instanceof Scene && a.context === "area") return -1;
        if (b.contextEntity instanceof Scene && b.context === "area") return 1;
      }
      if (
        a.contextEntity instanceof Scene &&
        a.context === "combat" &&
        b.contextEntity instanceof Scene &&
        b.context !== "combat"
      )
        return -1;
      if (
        b.contextEntity instanceof Scene &&
        b.context === "combat" &&
        a.contextEntity instanceof Scene &&
        a.context !== "combat"
      )
        return 1;
    }

    // Sort by priority
    return b.priority - a.priority;
  },

  getCurrentPlaylist() {
    const playlists = this.getAllCurrentPlaylists()
      .filter(this.filterPlaylists.bind(this))
      .sort(this.sortPlaylists.bind(this));
    if (playlists.length > 0) return playlists[0];

    return null;
  },

  async playCurrentTrack() {
    if (!isHeadGM()) return;

    const context = this.getCurrentPlaylist();

    let promises = [];
    const music = this.music;
    const isNotSameTrack = context?.playlist != null && context.playlist.id !== music?.playlist?.id;
    const shouldStopCurrentMusic = music != null && context?.playlist?.id !== music.id;
    // Save playlist data
    if (shouldStopCurrentMusic && this.context != null) await this.savePlaylistData(this.context.scopeEntity);
    // Switch music
    if (isNotSameTrack) promises.push(this.playMusic(context));
    // Stop current music
    if (shouldStopCurrentMusic) promises.push(music.stop());

    await Promise.all(promises);
  },

  getPlaylistData(entity, playlistId, trackId = null) {
    const playlistData = entity.getFlag("vgmusic", `playlist.${playlistId}`);
    if (!playlistData) {
      return {
        id: playlistId,
        trackId,
        start: 0,
      };
    }
    return playlistData;
  },

  savePlaylistData(entity) {
    if (entity instanceof Combat && game.combats.get(entity.id) == null) return;

    const music = this.music;
    if (music == null || entity == null) return;

    if (isHeadGM()) {
      return entity.setFlag("vgmusic", `playlist.${music.id}`, {
        id: music.id,
        trackId: music.trackId,
        start:
          ((music.track?.sound.currentTime ?? 0) + this.fadeDuration / 1000) % (music.track?.sound.duration ?? 100),
      });
    }
  },

  async playMusic(playlistContext) {
    const tgt = playlistContext?.playlist;
    if (!(tgt instanceof Playlist)) return;

    // Set variables
    const playlistData = this.getPlaylistData(
      playlistContext.scopeEntity,
      playlistContext.playlist.id,
      playlistContext.trackId,
    );
    if (playlistData) {
      this.music = new PlaylistMemory(playlistData.id, playlistData.trackId, playlistData.start);
    } else {
      this.music = new PlaylistMemory(tgt.id, tgt.trackId, tgt.start);
    }
    this.context = playlistContext;

    // Get data
    const playlist = this.music.playlist;
    const track = this.music.track ?? this.music.getInitialTrack();

    // Load sound
    await playlist.playSound(track);
    const playback = {
      loop: track.data.repeat,
      volume: track.volume,
      fade: this.fadeDuration,
      offset: this.music.start ?? 0,
    };
    this.sync({ playlistId: tgt.id, trackId: tgt.trackId, playbackData: playback });
    game.VGMusic._firstPlay = true;
  },

  sync({ playlistId = null, trackId = null, playbackData = null } = {}) {
    if (!game.user.isGM) return;
    if (!game.VGMusic._firstPlay) return;

    if (!playlistId) playlistId = this.music?.id;
    if (!trackId) trackId = this.music?.trackId;
    if (!playbackData) {
      const track = this.music?.track;
      if (!track) return;
      playbackData = {
        loop: track.data.repeat,
        volume: track.volume,
        fade: 0,
        offset: this.music.track?.sound.currentTime || this.music.start || 0,
      };
    }

    game.socket.emit("module.vgmusic", { type: "sync", args: [playlistId, trackId, playbackData, 5000] });
    SyncStorage.add(playlistId, trackId, playbackData, 5000);
  },
};

export class PlaylistMemory {
  constructor(playlistId, trackId, start = null) {
    this.id = playlistId;
    this.trackId = trackId ?? this.getInitialTrack().id;
    this.start = start ?? 0;
  }

  get playlist() {
    return game.playlists.get(this.id);
  }

  get track() {
    if (this.trackId != null) return this.playlist.sounds.get(this.trackId);
    return this.playlist.sounds.find((o) => o.playing);
  }

  getInitialTrack() {
    const playlist = this.playlist;
    if (!playlist) return;

    // Get default order
    const playbackOrder = playlist.playbackOrder;
    return this.playlist.sounds.get(playbackOrder[0]);
  }

  async stop() {
    const sound = this.track?.sound;
    if (sound) {
      if (MusicController.music === this) {
        MusicController.music = null;
      }
      await sound.fade(0, { duration: this.fadeDuration });
    }
    if (MusicController.music?.id !== this.id) {
      await this.playlist.stopAll();
    }
  }
}
