/**
 * This is your JavaScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module.
 */

// Import JavaScript modules
import { registerSettings } from "./settings.js";
import { preloadTemplates } from "./preloadTemplates.js";
import { patchActorSheet } from "./patch/actor-sheet.js";
import { patchCombat } from "./patch/combat.js";
import { patchSceneControls } from "./patch/scene-controls.js";
import { MusicController } from "./music-controller.js";
import { ActorMusicConfig } from "./apps/actor-music.js";
import { SceneMusicConfig } from "./apps/scene-music.js";
import { C } from "./config.js";
import { registerSockets } from "./socket.js";
import { PlaylistContext } from "./playlists.js";
import * as controls from "./controls.js";

// Initialize module
Hooks.once("init", async () => {
  if (typeof libWrapper !== "function") return;
  console.log("vgmusic | Initializing vgmusic");

  // Assign custom classes and constants here
  CONFIG.VGMusic = C;

  // Create game object
  game.VGMusic = {
    MusicController,
    PlaylistContext,
    // Configuration windows
    configClasses: {
      ActorMusicConfig,
      SceneMusicConfig,
    },
    // Track data
    _firstPlay: false,
    // Misc
    controls,
  };

  // Register custom module settings
  registerSettings();
  game.VGMusic.controls.register();

  // Preload Handlebars templates
  await preloadTemplates();

  // Register custom sheets (if any)
  registerSockets();
});

Hooks.once("setup", () => {
  // Translate config strings
  const sections = ["scenePlaylistSections", "actorPlaylistSections"];
  for (const sec of sections) {
    for (const [k, v] of Object.entries(getProperty(CONFIG.VGMusic, sec))) {
      setProperty(CONFIG.VGMusic, `${sec}.${k}`, game.i18n.localize(v));
    }
  }
});

// When ready
Hooks.once("ready", async () => {
  // Do anything once the module is ready
  if (typeof libWrapper !== "function") return;

  // Patch stuff
  patchActorSheet();
  patchCombat();
  patchSceneControls();

  // Emit socket to sync with GM
  game.socket.emit("module.vgmusic", { type: "connect" });
});

// Add any additional hooks if necessary
Hooks.on("updateCombat", (combat, updateData) => {
  // If the combat turn is changed
  if (combat.started && (updateData.turn != null || updateData.round != null)) {
    MusicController.playCurrentTrack();
  }
});

Hooks.on("deleteCombat", () => {
  MusicController.playCurrentTrack();
});

Hooks.on("renderSceneConfig", (app, html) => {
  // Disable core stuff
  const elem = html.find(`select[name="playlistSound"]`).parent();

  const elemStr =
    `<button type="button" data-action="vgmusic-scene"><i class="fas fa-music"></i>` +
    `${game.i18n.localize("VGMusic.SceneMusic")}</button>`;
  elem.after(elemStr);

  html.find(`button[data-action="vgmusic-scene"]`).on("click", (event) => {
    event.preventDefault();
    new SceneMusicConfig(app.object).render(true);
  });
});

Hooks.on("canvasReady", () => {
  MusicController.playCurrentTrack();
});

Hooks.on("updateScene", async (scene, updateData) => {
  if ("active" in updateData) {
    if (updateData.active !== true) {
      await scene.unsetFlag("vgmusic", "playlist");
    }
    MusicController.playCurrentTrack();
  }
});
