# 0.1.0

## Changes

- Added keyboard shortcuts for supressing area and combat music

## API

- Added the `game.VGMusic.PlaylistContext` class, used for determining the current playlist
- `game.VGMusic.MusicController.getAllCurrentPlaylists` now returns an array of `PlaylistContext`
